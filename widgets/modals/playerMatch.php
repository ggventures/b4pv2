    <div class="portfolio-modal modal fade" id="playerMatch" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="close-modal" data-dismiss="modal">
            <div class="lr">
              <div class="rl"></div>
            </div>
          </div>
          <div class="container">
            <div class="row">
              <div class="col-lg-8 mx-auto">
                <div class="modal-body">
                  <!-- Project Details Go Here -->
                  <h2 class="text-uppercase">Player Match</h2>
                  <p class="item-intro text-muted">Are you a club looking for an extra player or two this weekend?</p>
                  <img class="img-fluid d-block mx-auto" src="img/portfolio/01-full.jpg" alt="">
                  <p>For abolutely no cost to you or your club you can have intant access to a whole database full of players looking for games. Request as many as you like and they will be sent instant text messages and emails with your request. We'll keep in touch to let you know how your requests are getting on and you can track their progress at any time in your online account.</p>
                  <button class="btn btn-green" data-dismiss="modal" type="button">
                    <i class="fa fa-times"></i>
                    Close Project</button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    