    <div class="portfolio-modal modal fade" id="clubMatch" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="close-modal" data-dismiss="modal">
            <div class="lr">
              <div class="rl"></div>
            </div>
          </div>
          <div class="container">
            <div class="row">
              <div class="col-lg-8 mx-auto">
                <div class="modal-body">
                  <!-- Project Details Go Here -->
                  <h2 class="text-uppercase">Club Match</h2>
                  <p class="item-intro text-muted">Are you a player looking for a match this weekend?</p>
                  <img class="img-fluid d-block mx-auto" src="img/portfolio/03-full.jpg" alt="">
                  <p>We did some magic and now you can instantly search and request for a match this weekend, anywhere in the UK! Simply login, search, request and you're good to go, you will be kept up to date with text messages and emails and be able to track the prohress of your requests online.</p>
                  <button class="btn btn-green" data-dismiss="modal" type="button">
                    <i class="fa fa-times"></i>
                    Close Project</button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>