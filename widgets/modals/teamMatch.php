    <div class="portfolio-modal modal fade" id="teamMatch" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="close-modal" data-dismiss="modal">
            <div class="lr">
              <div class="rl"></div>
            </div>
          </div>
          <div class="container">
            <div class="row">
              <div class="col-lg-8 mx-auto">
                <div class="modal-body">
                  <!-- Project Details Go Here -->
                  <h2 class="text-uppercase">Team Match</h2>
                  <p class="item-intro text-muted">Got a group of friends all looking for a game?</p>
                  <img class="img-fluid d-block mx-auto" src="img/portfolio/02-full.jpg" alt="">
                  <p>Great! Simply create a team account with us and request games as your very own team. Either support a local side by bulking up their numbers or if you have enough players you could even organise a game of 7's against another team.</p>
                  <button class="btn btn-green" data-dismiss="modal" type="button">
                    <i class="fa fa-times"></i>
                    Close Project</button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    