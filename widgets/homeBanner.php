    <header class="masthead">
      <div class="container">
        <div class="intro-text">
          <div class="intro-lead-in">because Saturday's a rugby day</div>
          <div class="intro-heading text-uppercase">Let's get you playing!</div>
          <a class="btn btn-green btn-xl text-uppercase" href="#services">Tell Me More</a>
        </div>
      </div>
    </header>
    