    <section class="bg-light" id="portfolio">
      <div class="container">
        <div class="row">
          <div class="col-lg-12 text-center">
            <h2 class="section-heading text-uppercase">SERVICES</h2>
            <h3 class="section-subheading text-muted">Lorem ipsum dolor sit amet consectetur.</h3>
          </div>
        </div>
        <div class="row">
          <div class="col-md-4 col-sm-6 portfolio-item">
            <a class="portfolio-link" data-toggle="modal" href="#playerMatch">
              <div class="portfolio-hover">
                <div class="portfolio-hover-content">
                  <i class="fa fa-plus fa-3x"></i>
                </div>
              </div>
              <img class="img-fluid" src="<?php echo IMAGE_URL; ?>lonely.png" alt="">
            </a>
            <div class="portfolio-caption text-green">
              <h4>Player Match</h4>
              <p class="text-muted">Illustration</p>
            </div>
          </div>
          <div class="col-md-4 col-sm-6 portfolio-item">
            <a class="portfolio-link" data-toggle="modal" href="#teamMatch">
              <div class="portfolio-hover">
                <div class="portfolio-hover-content">
                  <i class="fa fa-plus fa-3x"></i>
                </div>
              </div>
              <img class="img-fluid" src="<?php echo IMAGE_URL; ?>excited.png" alt="">
            </a>
            <div class="portfolio-caption text-green">
              <h4>Team Match</h4>
              <p class="text-muted">Graphic Design</p>
            </div>
          </div>
          <div class="col-md-4 col-sm-6 portfolio-item">
            <a class="portfolio-link" data-toggle="modal" href="#clubMatch">
              <div class="portfolio-hover">
                <div class="portfolio-hover-content">
                  <i class="fa fa-plus fa-3x"></i>
                </div>
              </div>
              <img class="img-fluid" src="<?php echo IMAGE_URL; ?>idea.png" alt="">
            </a>
            <div class="portfolio-caption text-green">
              <h4>Club Match</h4>
              <p class="text-muted">Identity</p>
            </div>
          </div>
        </div>
      </div>
    </section>

<?php
require_once 'modals/clubMatch.php';
require_once 'modals/playerMatch.php';
require_once 'modals/teamMatch.php';
?>