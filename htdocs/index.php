<?php
require_once '../config/startup.php';
require_once '../includes/head.php';
?>
<body id="page-top">
<?php
require_once "../widgets/siteNavigation.php";
?>

<?php
require_once "../widgets/homeBanner.php";
?>

<?php
require_once "../widgets/homeWhatWeOffer.php";
?>

<?php
require_once "../widgets/homeServicesIcons.php";
?>

<?php
require_once "../widgets/sitePartners.php";
?>

<?php
require_once "../widgets/siteContactUs.php";
?>

<?php
require_once "../widgets/siteFooter.php";
?>
  </body>
</html>
