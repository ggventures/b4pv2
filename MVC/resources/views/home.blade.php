@extends('layouts.layout')

@section('content')
<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-md-3">
        @include('layouts.leftColumn')
        </div>

        <div class="col-md-6">
        @include('layouts.middleColumn')
        </div>

        <div class="col-md-3">
        @include('layouts.rightColumn')
        </div>  
    </div>
</div>
@endsection
